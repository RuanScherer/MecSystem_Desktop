package mecsystem;

import Activitys.actLogin;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.Thread.sleep;
import mecsystem.Classes.Company;
import mecsystem.Classes.Connection;
    
public class MecSystem {

    public static void main(String[] args) {
        Connection con = new Connection();
        if (con.getConexao() != null) {
            System.out.println("Conectado");
        }

        Company c = new Company();
        c.setIdCompany(1);
        c.ReadById();
        if (c.getNameCompany() == null) {
            FileWriter arq;
            try {
                String MachineName = System.getProperty("user.name");
                arq = new FileWriter("C:/MecSystem/monthControl.bat");
                PrintWriter gravarArq = new PrintWriter(arq);

                gravarArq.print("copy /y monthControl.bat \"C:\\Users\\" + MachineName + "\\AppData\\Roaming\\Microsoft\\Windows\\Start Menu\\Programs\\Startup\"\n\n"
                        + "java -jar C:\\MecSystem\\dist\\MSMonthControl.jar");

                arq.close();
                sleep(100);
            } catch (IOException ex) {
            } catch (InterruptedException ex) {
            }
            new Activitys.actFirstLogin().setVisible(true);
        } else {
            new actLogin().setVisible(true);
        }
    }

}
