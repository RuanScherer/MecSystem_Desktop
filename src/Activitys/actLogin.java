/*
 * ControlePonto_v2.0
 * Cedup Hermmann Hering 20/04/19
 * @MecSystem
 */
package Activitys;

import java.awt.Color;
import java.awt.Cursor;
import mecsystem.Classes.User;

/**
 *
 * @author LOL
 */
public class actLogin extends javax.swing.JFrame {

    /**
     * Creates new form actLogin
     */
    boolean isAdmin;
    String loginName = "";

    public actLogin() {
        initComponents();
        this.setIconImage(new javax.swing.ImageIcon(getClass().getResource("/Activitys/images/icone.png")).getImage());
        Desfoque.setVisible(false);
        this.setLocationRelativeTo(null);   //Centraliza a Tela
        imgBackground.requestFocus();
        btnPronto.setVisible(false);
        Desfoque.setBackground(new Color(0, 0, 0, 100));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Desfoque = new javax.swing.JPanel();
        lbCPF = new javax.swing.JLabel();
        btnPronto = new javax.swing.JLabel();
        imgPronto = new javax.swing.JLabel();
        btnSair = new javax.swing.JLabel();
        edtPassword = new javax.swing.JPasswordField();
        edtCPF = new javax.swing.JFormattedTextField();
        imgBackground = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        addWindowFocusListener(new java.awt.event.WindowFocusListener() {
            public void windowGainedFocus(java.awt.event.WindowEvent evt) {
                formWindowGainedFocus(evt);
            }
            public void windowLostFocus(java.awt.event.WindowEvent evt) {
                formWindowLostFocus(evt);
            }
        });
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        getContentPane().add(Desfoque, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1028, 720));

        lbCPF.setBackground(new java.awt.Color(59, 146, 223));
        lbCPF.setFont(new java.awt.Font("Microsoft YaHei UI", 0, 22)); // NOI18N
        lbCPF.setForeground(new java.awt.Color(255, 255, 255));
        lbCPF.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbCPF.setText("CPF");
        lbCPF.setOpaque(true);
        getContentPane().add(lbCPF, new org.netbeans.lib.awtextra.AbsoluteConstraints(449, 225, 130, -1));

        btnPronto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Activitys/images/BotaoProntoLogin2.jpg"))); // NOI18N
        btnPronto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnProntoMouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnProntoMouseExited(evt);
            }
        });
        getContentPane().add(btnPronto, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 405, -1, -1));

        imgPronto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Activitys/images/BotaoProntoLogin.jpg"))); // NOI18N
        imgPronto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                imgProntoMouseEntered(evt);
            }
        });
        getContentPane().add(imgPronto, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 405, -1, -1));

        btnSair.setFont(new java.awt.Font("Microsoft YaHei Light", 0, 24)); // NOI18N
        btnSair.setForeground(new java.awt.Color(255, 255, 255));
        btnSair.setText("Sair");
        btnSair.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnSairMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSairMouseEntered(evt);
            }
        });
        getContentPane().add(btnSair, new org.netbeans.lib.awtextra.AbsoluteConstraints(980, 3, -1, -1));

        edtPassword.setBackground(new java.awt.Color(51, 170, 236));
        edtPassword.setFont(new java.awt.Font("Microsoft YaHei UI", 0, 18)); // NOI18N
        edtPassword.setForeground(new java.awt.Color(255, 255, 255));
        edtPassword.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        edtPassword.setText("Senha");
        edtPassword.setToolTipText("Senha");
        edtPassword.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(255, 255, 255), 1, true));
        edtPassword.setOpaque(false);
        edtPassword.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                edtPasswordFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                edtPasswordFocusLost(evt);
            }
        });
        edtPassword.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edtPasswordActionPerformed(evt);
            }
        });
        getContentPane().add(edtPassword, new org.netbeans.lib.awtextra.AbsoluteConstraints(385, 280, 260, 40));

        edtCPF.setBackground(new java.awt.Color(51, 170, 236));
        edtCPF.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(255, 255, 255), 1, true));
        edtCPF.setForeground(new java.awt.Color(255, 255, 255));
        try {
            edtCPF.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("###.###.###-##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        edtCPF.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        edtCPF.setToolTipText("CPF");
        edtCPF.setFont(new java.awt.Font("Microsoft YaHei UI", 0, 18)); // NOI18N
        edtCPF.setOpaque(false);
        edtCPF.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                edtCPFFocusGained(evt);
            }
        });
        getContentPane().add(edtCPF, new org.netbeans.lib.awtextra.AbsoluteConstraints(385, 220, 260, 40));

        imgBackground.setBackground(new java.awt.Color(59, 146, 223));
        imgBackground.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Activitys/images/Tela-Login.jpg"))); // NOI18N
        imgBackground.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                imgBackgroundMouseClicked(evt);
            }
        });
        getContentPane().add(imgBackground, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnProntoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnProntoMouseClicked
        if (doValidations()) {
            actHome h = new actHome();
            h.setLogin(loginName);
            this.dispose();
            h.setVisible(true);
        }
    }//GEN-LAST:event_btnProntoMouseClicked

    private void btnProntoMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnProntoMouseExited
        btnPronto.setVisible(false);
        imgPronto.setVisible(true);
    }//GEN-LAST:event_btnProntoMouseExited

    private void imgProntoMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_imgProntoMouseEntered
        imgPronto.setVisible(false);
        btnPronto.setVisible(true);
        btnPronto.setCursor(new Cursor(Cursor.HAND_CURSOR));
    }//GEN-LAST:event_imgProntoMouseEntered

    private void btnSairMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSairMouseClicked
        this.dispose();
    }//GEN-LAST:event_btnSairMouseClicked

    private void btnSairMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSairMouseEntered
        btnSair.setCursor(new Cursor(Cursor.HAND_CURSOR));
    }//GEN-LAST:event_btnSairMouseEntered

    private void edtPasswordFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_edtPasswordFocusGained
        edtPassword.setOpaque(true);
        if (edtPassword.getText().equals("Senha")) {
            edtPassword.setText("");
        }
    }//GEN-LAST:event_edtPasswordFocusGained

    private void edtPasswordFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_edtPasswordFocusLost
        if (edtPassword.getText().trim().isEmpty()) {
            edtPassword.setOpaque(false);
            edtPassword.setText("Senha");
        }
    }//GEN-LAST:event_edtPasswordFocusLost

    private void imgBackgroundMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_imgBackgroundMouseClicked
        imgBackground.requestFocus();
    }//GEN-LAST:event_imgBackgroundMouseClicked

    private void formWindowLostFocus(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowLostFocus
        Desfoque.setVisible(true);
    }//GEN-LAST:event_formWindowLostFocus

    private void formWindowGainedFocus(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowGainedFocus
        Desfoque.setVisible(false);

    }//GEN-LAST:event_formWindowGainedFocus

    private void edtCPFFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_edtCPFFocusGained
        edtCPF.setOpaque(true);
        lbCPF.setVisible(false);
    }//GEN-LAST:event_edtCPFFocusGained

    private void edtPasswordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edtPasswordActionPerformed
        this.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        if (doValidations()) {
            this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            actHome h = new actHome();
            h.setLogin(loginName);
            this.dispose();
            h.setVisible(true);
        }
    }//GEN-LAST:event_edtPasswordActionPerformed

    public boolean CheckIsEmpty() {
        if ((edtCPF.getText().equals("..-") || edtCPF.getText().equals("   .   .   -  ") || (edtCPF.getText().length() != 14))) {
            return false;
        } else if ((edtPassword.getText().trim().isEmpty() || edtPassword.getText().equals("Senha"))) {
            return false;
        }
        return true;
    }

    public boolean doValidations() {
        if (!CheckIsEmpty()) {
            new actAlert().setVisible(true);
            return false;
        } else if (!DoLogin()) {
            actAlert a = new actAlert();
            a.lbMsg.setText("<html><p align ='center'> Login ou Senha incorretos!</p></html>");
            a.setVisible(true);
            return false;
        } else if (!isAdmin) {
            actAlert a = new actAlert();
            a.lbMsg.setText("<html><p align ='center'> Esse usuário não tem permissão de acesso!</p></html>");
            a.setVisible(true);
            return false;
        } else {
            return true;
        }
    }

    public boolean DoLogin() {
        User u = new User();
        u.setCPF(edtCPF.getText());
        u.setPassword(edtPassword.getText());
        u.doLoging();
        loginName = (u.getCPF());
        isAdmin = (u.getIsAdmin());
        
        return u.getPassword()!= null;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(actLogin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(actLogin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(actLogin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(actLogin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new actLogin().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel Desfoque;
    private javax.swing.JLabel btnPronto;
    private javax.swing.JLabel btnSair;
    private javax.swing.JFormattedTextField edtCPF;
    private javax.swing.JPasswordField edtPassword;
    private javax.swing.JLabel imgBackground;
    private javax.swing.JLabel imgPronto;
    private javax.swing.JLabel lbCPF;
    // End of variables declaration//GEN-END:variables
}
