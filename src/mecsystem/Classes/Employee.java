package mecsystem.Classes;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Employee extends User {

    private int userId;
    private String name;
    private String lastname;
    private String RG;
    private String PIS;
    private String FunctionName;
    private String CardNumber;
    private String Email;

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }
    private boolean Admin;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
     
    public boolean isAdmin() {
        return Admin;
    }

    public void setAdmin(boolean Admin) {
        this.Admin = Admin;
    }

    public String getCardNumber() {
        return CardNumber;
    }

    public void setCardNumber(String CardNumber) {
        this.CardNumber = CardNumber;
    }
    Connection con = new Connection();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getRG() {
        return RG;
    }

    public void setRG(String RG) {
        this.RG = RG;
    }

    public String getPIS() {
        return PIS;
    }

    public void setPIS(String PIS) {
        this.PIS = PIS;
    }

    public String getFunctionName() {
        return FunctionName;
    }

    public void setFunctionName(String FunctionName) {
        this.FunctionName = FunctionName;
    }

    public boolean isPis() {
        char[] digit = this.PIS.toCharArray();
        int x1 = 3 * Character.getNumericValue(digit[0]);
        int x2 = 2 * Character.getNumericValue(digit[1]);
        int x3 = 9 * Character.getNumericValue(digit[2]);
        int x4 = 8 * Character.getNumericValue(digit[4]);
        int x5 = 7 * Character.getNumericValue(digit[5]);
        int x6 = 6 * Character.getNumericValue(digit[6]);
        int x7 = 5 * Character.getNumericValue(digit[7]);
        int x8 = 4 * Character.getNumericValue(digit[8]);
        int x9 = 3 * Character.getNumericValue(digit[10]);
        int x10 = 2 * Character.getNumericValue(digit[11]);

        int plus = x1 + x2 + x3 + x4 + x5 + x6 + x7 + x8 + x9 + x10;
        int divison = plus % 11;
        int minus = 11 - divison;
        if ((minus == 10) || (minus == 11)) {
            minus = 0;
        }

        return (Character.getNumericValue(digit[13]) == minus);

    }

    public Boolean FirstCreate() {
        String sql = "INSERT INTO tb_user ( `UserName`, `UserLastName`, `UserCPF`, `RG`, `CardNumber`, `UserPassword`, `fk_function`, `Admin`, `UserEmail`) "
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement stm;
        try {
            stm = con.getConexao().prepareStatement(sql);
            stm.setString(1, this.name);
            stm.setString(2, this.lastname);
            stm.setString(3, this.getCPF());
            stm.setString(4, this.RG);
            stm.setString(5, this.CardNumber);
            stm.setString(6, this.getPassword());
            stm.setInt(7, 1);
            stm.setBoolean(8, this.Admin);
            stm.setString(9, this.Email);
            stm.execute();
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }

    public void VerifyCPFExists() {
        ResultSet rs = null;
        java.sql.Connection conect = null;
        conect = con.getConexao();
        String sql = "SELECT * FROM tb_user where UserCpf = ? ";
        PreparedStatement stm;
        try {
            stm = conect.prepareStatement(sql);
            stm.setString(1, this.getCPF());
            rs = stm.executeQuery();
            while (rs.next()) {
                this.name = rs.getString("UserName");
                this.userId =  rs.getInt("idtb_user");
            }
        } catch (SQLException ex) {
            System.out.println("Não Consultou"
                    + ex.getMessage());
        }

    }
    
    public void VerifyRGExists() {
        ResultSet rs = null;
        java.sql.Connection conect = null;
        conect = con.getConexao();
        String sql = "SELECT * FROM tb_user where RG = ? ";
        PreparedStatement stm;
        try {
            stm = conect.prepareStatement(sql);
            stm.setString(1, this.RG);
            rs = stm.executeQuery();
            while (rs.next()) {
                this.name = rs.getString("UserName");
                this.userId = rs.getInt("idtb_user");
            }
        } catch (SQLException ex) {
            System.out.println("Não Consultou"
                    + ex.getMessage());
        }

    }
    
    public Boolean Create() {
        String sql = "INSERT INTO tb_user ( `UserName`, `UserLastName`, `UserCPF`, `UserPassword`, `RG`, `PIS`, `CardNumber`,"
                + " `fk_function`, `Admin`,`UserEmail` ) VALUES (?, ?, ?, ?, ?, ?, ?,1,true, ?)";
        PreparedStatement stm;
        try {
            stm = con.getConexao().prepareStatement(sql);
            stm.setString(1, this.name);
            stm.setString(2, this.lastname);
            stm.setString(3, this.getCPF());
            stm.setString(4, this.getPassword());
            stm.setString(5, this.RG);
            stm.setString(6, this.PIS);
            stm.setString(7, this.CardNumber);
            stm.setString(8, this.Email);
            //stm.setString(8, this.FunctionName);
            //stm.setBoolean(9, this.Admin);
            stm.execute();
            return true;
        } catch (SQLException ex) {
            System.err.println(ex);
            return false;
            
        }
    }
    
    public Boolean Update() {
        String sql = "UPDATE `tb_user` SET `UserName` = ?, `UserLastName` = ?, `UserCpf` = ?, `RG` = ?,\n" +
"                `PIS` = ?, `CardNumber` = ?, `UserPassword` = ?, `UserEmail` = ?, `fk_function` = (SELECT idtb_function FROM tb_function\n" +
"                WHERE FunctionName = ?), `Admin` = ?\n" +
"                WHERE (`idtb_user` = ?)";
        PreparedStatement stm;
        try {
            stm = con.getConexao().prepareStatement(sql);
            stm.setString(1, this.name);
            stm.setString(2, this.lastname);
            stm.setString(3, this.getCPF());
            stm.setString(4, this.RG);
            stm.setString(5, this.PIS);
            stm.setString(6, this.CardNumber);
            stm.setString(7, this.getPassword());
            stm.setString(8, this.Email);
            stm.setString(9, this.FunctionName);
            stm.setBoolean(10, Admin);
            stm.setInt(11, this.userId);
            
            stm.execute();
            return true;
        } catch (SQLException ex) {
            System.err.println(ex);
            return false;
        }
    }
    
    public Boolean Delete() {
        String sql = "DELETE FROM `tb_user` WHERE (`idtb_user` = ?);";
        PreparedStatement stm;
        try {
            stm = con.getConexao().prepareStatement(sql);
            stm.setInt(1, this.userId);
            stm.execute();
            return true;
        } catch (SQLException ex) {
            return false;
        }
    }
    
    public ResultSet Read() {
        ResultSet rs = null;
        java.sql.Connection conect = null;
        conect = con.getConexao();
        String sql = "SELECT idtb_user, UserName, UserLastName, UserCpf, RG, PIS, CardNumber, UserPassword, "
                + "Admin, FunctionName FROM tb_user INNER JOIN tb_function ON idtb_function = fk_function WHERE UserName like ?";
        PreparedStatement stm;
        try {
            stm = conect.prepareStatement(sql);
            stm.setString(1, "%"+this.name+"%");
            rs = stm.executeQuery();
        } catch (SQLException ex) {
            System.out.println("Não Consultou"
                    + ex.getMessage());
        }
        return rs;
    }
    
    public void VerifyPISExists() {
        ResultSet rs = null;
        java.sql.Connection conect = null;
        conect = con.getConexao();
        String sql = "SELECT * FROM tb_user where PIS = ? ";
        PreparedStatement stm;
        try {
            stm = conect.prepareStatement(sql);
            stm.setString(1, this.PIS);
            rs = stm.executeQuery();
            while (rs.next()) {
                this.name = rs.getString("UserName");
                this.userId = rs.getInt("idtb_user");
            }
        } catch (SQLException ex) {
            System.out.println("Não Consultou"
                    + ex.getMessage());
        }

    }
    
    public void ReadByCPF() {
        ResultSet rs = null;
        java.sql.Connection conect = null;
        conect = con.getConexao();
        String sql = "SELECT idtb_user, UserName, UserLastName, UserCpf, RG, UserEmail, PIS, CardNumber, UserPassword, "
                + "Admin, FunctionName FROM tb_user INNER JOIN tb_function ON idtb_function = fk_function WHERE UserCpf = ?";
        PreparedStatement stm;
        try {
            stm = conect.prepareStatement(sql);
            stm.setString(1, this.getCPF());
            rs = stm.executeQuery();
            while (rs.next()) {
                this.setIsAdmin(rs.getBoolean("Admin"));
                this.CardNumber = rs.getString("CardNumber");
                this.FunctionName = rs.getString("FunctionName");
                this.PIS = rs.getString("PIS");
                this.RG = rs.getString("RG");
                this.lastname = rs.getString("UserLastName");
                this.name = rs.getString("UserName");
                this.setPassword(rs.getString("Userpassword"));
                this.userId = rs.getInt("idtb_user");
                this.Email = rs.getString("UserEmail");
            }
        } catch (SQLException ex) {
            System.out.println("Não Consultou"
                    + ex.getMessage());
        }
    }
}
