package mecsystem.Classes;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


/*while (rs.next()) {
            String[] DateArray = rs.getString("Date").split("-");
            this.Date = (DateArray[2] + "/" + DateArray[1] + "/" + DateArray[0]);
            this.DepartureTime = rs.getString("DepartureTime");
            this.EletronicPoint = rs.getString("Locale");
            this.EmployeerName = rs.getString("UserName");
            this.EmployeerLastname = rs.getString("UserLastname");
            this.EntryHour = rs.getString("EntryHour");
            this.LunchBreak = rs.getString("LunchBreak");
            this.LunchReturn = rs.getString("LunchReturn");
            }*/

public class Point {

    private String EntryHour;
    private String LunchBreak;
    private String LunchReturn;
    private String DepartureTime;
    private String Date;
    private String EletronicPoint;
    private String EmployeerName;
    private String EmployeerLastname;
    Connection con = new Connection();

    public String getEntryHour() {
        return EntryHour;
    }

    public void setEntryHour(String EntryHour) {
        this.EntryHour = EntryHour;
    }

    public String getLunchBreak() {
        return LunchBreak;
    }

    public void setLunchBreak(String LunchBreak) {
        this.LunchBreak = LunchBreak;
    }

    public String getLunchReturn() {
        return LunchReturn;
    }

    public void setLunchReturn(String LunchReturn) {
        this.LunchReturn = LunchReturn;
    }

    public String getDepartureTime() {
        return DepartureTime;
    }

    public void setDepartureTime(String DepartureTime) {
        this.DepartureTime = DepartureTime;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String Date) {
        this.Date = Date;
    }

    public String getEletronicPoint() {
        return EletronicPoint;
    }

    public void setEletronicPoint(String EletronicPoint) {
        this.EletronicPoint = EletronicPoint;
    }

    public String getEmployeerName() {
        return EmployeerName;
    }

    public void setEmployeerName(String EmployeerName) {
        this.EmployeerName = EmployeerName;
    }

    public String getEmployeerLastname() {
        return EmployeerLastname;
    }

    public void setEmployeerLastname(String EmployeerLastname) {
        this.EmployeerLastname = EmployeerLastname;
    }


    public ResultSet ReadByName() {
        ResultSet rs = null;
        java.sql.Connection conect = null;
        conect = con.getConexao();
        String sql = "SELECT EntryHour, EntryHourManual, LunchBreak, LunchBreakManual, LunchReturn, LunchReturnManual"
                + ", DepartureTime, DepartureTimeManual, Date, Locale,UserCPF, UserName, UserLastName\n"
                + "FROM tb_point INNER JOIN tb_user on fk_employee = idtb_user INNER JOIN tb_electronicPoint on\n"
                + "fk_electronicpoint = idtb_electronicPoint WHERE UserName LIKE ?;";
        PreparedStatement stm;
        try {
            stm = conect.prepareStatement(sql);
            stm.setString(1, "%" + this.EmployeerName + "%");
            rs = stm.executeQuery();           
        } catch (SQLException ex) {
            System.out.println("Não Consultou"
                    + ex.getMessage());
        }
        return rs;
    }

    public ResultSet ReadByFullName() {
        ResultSet rs = null;
        java.sql.Connection conect = null;
        conect = con.getConexao();
        String sql = "SELECT EntryHour, EntryHourManual, LunchBreak, LunchBreakManual, LunchReturn, LunchReturnManual"
                + ", DepartureTime, DepartureTimeManual, Date, Locale,UserCPF, UserName, UserLastName\n"
                + "FROM tb_point INNER JOIN tb_user on fk_employee = idtb_user INNER JOIN tb_electronicPoint on\n"
                + "fk_electronicpoint = idtb_electronicPoint WHERE UserName like ? AND UserLastName like ?;";
        PreparedStatement stm;
        try {
            stm = conect.prepareStatement(sql);
            stm.setString(1, "%"+this.EmployeerName + "%");
            stm.setString(2, "%" + this.EmployeerLastname + "%");
            rs = stm.executeQuery();           
        } catch (SQLException ex) {
            System.out.println("Não Consultou"
                    + ex.getMessage());
        }
        return rs;
    }
    
    public ResultSet ReadByDate() {
        ResultSet rs = null;
        java.sql.Connection conect = null;
        conect = con.getConexao();
        String sql = "SELECT EntryHour, EntryHourManual, LunchBreak, LunchBreakManual, LunchReturn, LunchReturnManual"
                + ", DepartureTime, DepartureTimeManual, Date, Locale,UserCPF, UserName, UserLastName\n"
                + "FROM tb_point INNER JOIN tb_user on fk_employee = idtb_user INNER JOIN tb_electronicPoint on\n"
                + "fk_electronicPoint = idtb_electronicPoint WHERE Date = ?;";
        PreparedStatement stm;
        try {
            stm = conect.prepareStatement(sql);
            stm.setString(1,this.Date);
            rs = stm.executeQuery();           
        } catch (SQLException ex) {
            System.out.println("Não Consultou"
                    + ex.getMessage());
        }
        return rs;
    }
    
    public ResultSet ReadByDateAndName() {
        ResultSet rs = null;
        java.sql.Connection conect = null;
        conect = con.getConexao();
        String sql = "SELECT EntryHour, EntryHourManual, LunchBreak, LunchBreakManual, LunchReturn, LunchReturnManual"
                + ", DepartureTime, DepartureTimeManual, Date, Locale, UserName,UserCPF, UserLastName\n"
                + "FROM tb_point INNER JOIN tb_user on fk_employee = idtb_user INNER JOIN tb_electronicPoint on\n"
                + "fk_electronicPoint = idtb_electronicPoint WHERE UserName like ? AND Date = ?;";
        PreparedStatement stm;
        try {
            stm = conect.prepareStatement(sql);
            stm.setString(2,this.Date);
            stm.setString(1, "%" + this.EmployeerName + "%");
            rs = stm.executeQuery();           
        } catch (SQLException ex) {
            System.out.println("Não Consultou"
                    + ex.getMessage());
        }
        return rs;
    }
    
    public ResultSet ReadByDateAndFullName() {
        ResultSet rs = null;
        java.sql.Connection conect = null;
        conect = con.getConexao();
        String sql = "SELECT EntryHour, EntryHourManual, LunchBreak, LunchBreakManual, LunchReturn, LunchReturnManual"
                + ", DepartureTime, DepartureTimeManual, Date, Locale,UserCPF , UserName, UserLastName\n"
                + "FROM tb_point INNER JOIN tb_user on fk_employee = idtb_user INNER JOIN tb_electronicPoint on\n"
                + "fk_electronicPoint = idtb_electronicPoint WHERE Date  = ? AND UserName like ? AND UserLastName LIKE ?;";
        PreparedStatement stm;
        try {
            stm = conect.prepareStatement(sql);
            stm.setString(1,this.Date);
            stm.setString(2,"%" + this.EmployeerName  + "%");
            stm.setString(3, "%" + this.EmployeerLastname + "%");
            rs = stm.executeQuery();           
        } catch (SQLException ex) {
            System.out.println("Não Consultou"
                    + ex.getMessage());
        }
        return rs;
    }
    
    public ResultSet ReadByMonthAndName() {
        ResultSet rs = null;
        java.sql.Connection conect = null;
        conect = con.getConexao();
        String sql = "SELECT EntryHour, EntryHourManual, LunchBreak, LunchBreakManual, LunchReturn, LunchReturnManual"
                + ", DepartureTime, DepartureTimeManual, Date, Locale,UserCPF, UserName, UserLastName\n"
                + "FROM tb_point INNER JOIN tb_user on fk_employee = idtb_user INNER JOIN tb_electronicPoint on\n"
                + "fk_electronicPoint = idtb_electronicPoint WHERE month(Date) = ? AND year(Date) = ? AND UserName LIKE ?;";
        PreparedStatement stm;
        try {
            String[] dates = this.Date.split("/");
            stm = conect.prepareStatement(sql);
            stm.setString(1,dates[1]);
            stm.setString(2,"20"+dates[0]);
            stm.setString(3, "%" + this.EmployeerName + "%");
            rs = stm.executeQuery();           
        } catch (SQLException ex) {
            System.out.println("Não Consultou"
                    + ex.getMessage());
        }
        return rs;
    }
    
    public ResultSet ReadByMonth() {
        ResultSet rs = null;
        java.sql.Connection conect = null;
        conect = con.getConexao();
        String sql = "SELECT EntryHour, EntryHourManual, LunchBreak, LunchBreakManual, LunchReturn, LunchReturnManual"
                + ", DepartureTime, DepartureTimeManual, Date, Locale,UserCPF, UserName, UserLastName\n"
                + "FROM tb_point INNER JOIN tb_user on fk_employee = idtb_user INNER JOIN tb_electronicPoint on\n"
                + "fk_electronicPoint = idtb_electronicPoint WHERE month(Date) = ? AND year(Date) = ?;";
        PreparedStatement stm;
        try {
            String[] dates = this.Date.split("/");
            stm = conect.prepareStatement(sql);
            stm.setString(1,dates[1]);
            stm.setString(2,"20"+dates[0]);
            rs = stm.executeQuery();           
        } catch (SQLException ex) {
            System.out.println("Não Consultou"
                    + ex.getMessage());
        }
        return rs;
    }
    
    public ResultSet ReadByMonthAndFullname() {
        ResultSet rs = null;
        java.sql.Connection conect = null;
        conect = con.getConexao();
        String sql = "SELECT EntryHour, EntryHourManual, LunchBreak, LunchBreakManual, LunchReturn, LunchReturnManual"
                + ", DepartureTime, DepartureTimeManual, Date, Locale,UserCPF, UserName, UserLastName\n"
                + "FROM tb_point INNER JOIN tb_user on fk_employee = idtb_user INNER JOIN tb_electronicPoint on\n"
                + "fk_electronicPoint = idtb_electronicPoint WHERE month(Date) = ? AND year(Date) = ? AND UserName like ? AND UserLastname LIKE ?;";
        PreparedStatement stm;
        try {
            String[] dates = this.Date.split("/");
            stm = conect.prepareStatement(sql);
            stm.setString(1,dates[1]);
            stm.setString(2,"20"+dates[0]);
            stm.setString(3, "%" + this.EmployeerName + "%");
            stm.setString(4, "%" + this.EmployeerLastname + "%");
            rs = stm.executeQuery();           
        } catch (SQLException ex) {
            System.out.println("Não Consultou"
                    + ex.getMessage());
        }
        return rs;
    }
    
    public Boolean UpdateEntry(){
            String sql = "UPDATE tb_point SET EntryHour = ?, EntryHourManual = true "
                    + "WHERE fk_employee = (SELECT idtb_user from tb_user "
                    + "WHERE UserName like? AND UserLastName like ?) AND Date = ?";
            PreparedStatement stm;
            try {
                stm = con.getConexao().prepareStatement(sql);
                stm.setString(1, this.EntryHour);
                stm.setString(2, "%" + this.EmployeerName + "%");
                stm.setString(3, "%" + this.EmployeerLastname + "%");
                stm.setString(4, this.Date);
                stm.execute();
                return true;
            }catch (SQLException ex){
                return false;
            }
    }
    
    public Boolean UpdateLunchBreak(){
            String sql = "UPDATE tb_point SET LunchBreak = ?, LunchBreakManual = true "
                    + "WHERE fk_employee = (SELECT idtb_user from tb_user "
                    + "WHERE UserName like? AND UserLastName like ?) AND Date = ?";
            PreparedStatement stm;
            try {
                stm = con.getConexao().prepareStatement(sql);
                stm.setString(1, this.LunchBreak);
                stm.setString(2, "%" + this.EmployeerName + "%");
                stm.setString(3, "%" + this.EmployeerLastname + "%");
                stm.setString(4, this.Date);
                stm.execute();
                return true;
            }catch (SQLException ex){
                return false;
            }
    }
    
    public Boolean UpdateLunchReturn(){
            String sql = "UPDATE tb_point SET LunchReturn = ?, LunchReturnManual = true "
                    + "WHERE fk_employee = (SELECT idtb_user from tb_user "
                    + "WHERE UserName like? AND UserLastName like ?) AND Date = ?";
            PreparedStatement stm;
            try {
                stm = con.getConexao().prepareStatement(sql);
                stm.setString(1, this.LunchReturn);
                stm.setString(2, "%" + this.EmployeerName + "%");
                stm.setString(3, "%" + this.EmployeerLastname + "%");
                stm.setString(4, this.Date);
                stm.execute();
                return true;
            }catch (SQLException ex){
                return false;
            }
    }
    
    public Boolean UpdateDepartureTime(){
            String sql = "UPDATE tb_point SET DepartureTime = ?, DepartureTimeManual = true "
                    + "WHERE fk_employee = (SELECT idtb_user from tb_user "
                    + "WHERE UserName like? AND UserLastName like ?) AND Date = ?";
            PreparedStatement stm;
            try {
                stm = con.getConexao().prepareStatement(sql);
                stm.setString(1, this.DepartureTime);
                stm.setString(2, "%" + this.EmployeerName + "%");
                stm.setString(3, "%" + this.EmployeerLastname + "%");
                stm.setString(4, this.Date);
                stm.execute();
                return true;
            }catch (SQLException ex){
                return false;
            }
    }
    
    public Boolean UpdateEntryLunchBreak(){
            String sql = "UPDATE tb_point SET EntryHour = ?, EntryHourManual = true"
                    + ", LunchBreak = ?, LunchBreakManual = true "
                    + "WHERE fk_employee = (SELECT idtb_user from tb_user "
                    + "WHERE UserName like? AND UserLastName like ?) AND Date = ?";
            PreparedStatement stm;
            try {
                stm = con.getConexao().prepareStatement(sql);
                stm.setString(1, this.EntryHour);
                stm.setString(2, this.LunchBreak);
                stm.setString(3, "%" + this.EmployeerName + "%");
                stm.setString(4, "%" + this.EmployeerLastname + "%");
                stm.setString(5, this.Date);
                stm.execute();
                return true;
            }catch (SQLException ex){
                return false;
            }
    }
    
    public Boolean UpdateEntryLunchBreakLunchReturn(){
            String sql = "UPDATE tb_point SET EntryHour = ?, EntryHourManual = true"
                    + ", LunchBreak = ?, LunchBreakManual = true "
                    + ", LunchReturn = ?, LunchReturnManual = true "
                    + "WHERE fk_employee = (SELECT idtb_user from tb_user "
                    + "WHERE UserName like? AND UserLastName like ?) AND Date = ?";
            PreparedStatement stm;
            try {
                stm = con.getConexao().prepareStatement(sql);
                stm.setString(1, this.EntryHour);
                stm.setString(2, this.LunchBreak);
                stm.setString(3, this.LunchReturn);
                stm.setString(4, "%" + this.EmployeerName + "%");
                stm.setString(5, "%" + this.EmployeerLastname + "%");
                stm.setString(6, this.Date);
                stm.execute();
                return true;
            }catch (SQLException ex){
                return false;
            }
    }
    
    public Boolean UpdateEntryLunchBreakLunchReturnDepartureTime(){
            String sql = "UPDATE tb_point SET EntryHour = ?, EntryHourManual = true"
                    + ", LunchBreak = ?, LunchBreakManual = true "
                    + ", LunchReturn = ?, LunchReturnManual = true "
                    + ", DepartureTime = ?, DepartureTimeManual = true "
                    + "WHERE fk_employee = (SELECT idtb_user from tb_user "
                    + "WHERE UserName like? AND UserLastName like ?) AND Date = ?";
            PreparedStatement stm;
            try {
                stm = con.getConexao().prepareStatement(sql);
                stm.setString(1, this.EntryHour);
                stm.setString(2, this.LunchBreak);
                stm.setString(3, this.LunchReturn);
                stm.setString(4, this.DepartureTime);
                stm.setString(5, "%" + this.EmployeerName + "%");
                stm.setString(6, "%" + this.EmployeerLastname + "%");
                stm.setString(7, this.Date);
                stm.execute();
                return true;
            }catch (SQLException ex){
                return false;
            }
    }
    
    public Boolean UpdateEntryLunchBreakDepartureTime(){
            String sql = "UPDATE tb_point SET EntryHour = ?, EntryHourManual = true"
                    + ", LunchBreak = ?, LunchBreakManual = true "
                    + ", DepartureTime = ?, DepartureTimeManual = true "
                    + "WHERE fk_employee = (SELECT idtb_user from tb_user "
                    + "WHERE UserName like? AND UserLastName like ?) AND Date = ?";
            PreparedStatement stm;
            try {
                stm = con.getConexao().prepareStatement(sql);
                stm.setString(1, this.EntryHour);
                stm.setString(2, this.LunchBreak);
                stm.setString(3, this.DepartureTime);
                stm.setString(4, "%" + this.EmployeerName + "%");
                stm.setString(5, "%" + this.EmployeerLastname + "%");
                stm.setString(6, this.Date);
                stm.execute();
                return true;
            }catch (SQLException ex){
                return false;
            }
    }
    
    public Boolean UpdateEntryLunchReturn(){
            String sql = "UPDATE tb_point SET EntryHour = ?, EntryHourManual = true"
                    + ", LunchReturn = ?, LunchReturnManual = true "
                    + "WHERE fk_employee = (SELECT idtb_user from tb_user "
                    + "WHERE UserName like? AND UserLastName like ?) AND Date = ?";
            PreparedStatement stm;
            try {
                stm = con.getConexao().prepareStatement(sql);
                stm.setString(1, this.EntryHour);
                stm.setString(2, this.LunchReturn);
                stm.setString(3, "%" + this.EmployeerName + "%");
                stm.setString(4, "%" + this.EmployeerLastname + "%");
                stm.setString(5, this.Date);
                stm.execute();
                return true;
            }catch (SQLException ex){
                return false;
            }
    }
    
    public Boolean UpdateEntryLunchReturnDepartureTime(){
            String sql = "UPDATE tb_point SET EntryHour = ?, EntryHourManual = true"
                    + ", LunchReturn = ?, LunchReturnManual = true "
                    + ", DepartureTime = ?, DepartureTimeManual = true "
                    + "WHERE fk_employee = (SELECT idtb_user from tb_user "
                    + "WHERE UserName like? AND UserLastName like ?) AND Date = ?";
            PreparedStatement stm;
            try {
                stm = con.getConexao().prepareStatement(sql);
                stm.setString(1, this.EntryHour);
                stm.setString(2, this.LunchReturn);
                stm.setString(3, this.DepartureTime);
                stm.setString(4, "%" + this.EmployeerName + "%");
                stm.setString(5, "%" + this.EmployeerLastname + "%");
                stm.setString(6, this.Date);
                stm.execute();
                return true;
            }catch (SQLException ex){
                return false;
            }
    }  
    
    public Boolean UpdateEntryDepartureTime(){
            String sql = "UPDATE tb_point SET EntryHour = ?, EntryHourManual = true"
                    + ", DepartureTime = ?, DepartureTimeManual = true "
                    + "WHERE fk_employee = (SELECT idtb_user from tb_user "
                    + "WHERE UserName like? AND UserLastName like ?) AND Date = ?";
            PreparedStatement stm;
            try {
                stm = con.getConexao().prepareStatement(sql);
                stm.setString(1, this.EntryHour);
                stm.setString(2, this.DepartureTime);
                stm.setString(3, "%" + this.EmployeerName + "%");
                stm.setString(4, "%" + this.EmployeerLastname + "%");
                stm.setString(5, this.Date);
                stm.execute();
                return true;
            }catch (SQLException ex){
                return false;
            }
    }  
    
    public Boolean UpdateLunchBreakLunchReturn(){
            String sql = "UPDATE tb_point SET LunchBreak = ?, LunchBreakManual = true "
                    + ", LunchReturn = ?, LunchReturnManual = true "
                    + "WHERE fk_employee = (SELECT idtb_user from tb_user "
                    + "WHERE UserName like? AND UserLastName like ?) AND Date = ?";
            PreparedStatement stm;
            try {
                stm = con.getConexao().prepareStatement(sql);
                stm.setString(1, this.LunchBreak);
                stm.setString(2, this.LunchReturn);
                stm.setString(3, "%" + this.EmployeerName + "%");
                stm.setString(4, "%" + this.EmployeerLastname + "%");
                stm.setString(5, this.Date);
                stm.execute();
                return true;
            }catch (SQLException ex){
                return false;
            }
    }
    
    public Boolean UpdateLunchBreakLunchReturnDepartureTime(){
            String sql = "UPDATE tb_point SET LunchBreak = ?, LunchBreakManual = true "
                    + ", LunchReturn = ?, LunchReturnManual = true "
                    + ", DepartureTime = ?, DepartureTimeManual = true "
                    + "WHERE fk_employee = (SELECT idtb_user from tb_user "
                    + "WHERE UserName like? AND UserLastName like ?) AND Date = ?";
            PreparedStatement stm;
            try {
                stm = con.getConexao().prepareStatement(sql);
                stm.setString(1, this.LunchBreak);
                stm.setString(2, this.LunchReturn);
                stm.setString(3, this.DepartureTime);
                stm.setString(4, "%" + this.EmployeerName + "%");
                stm.setString(5, "%" + this.EmployeerLastname + "%");
                stm.setString(6, this.Date);
                stm.execute();
                return true;
            }catch (SQLException ex){
                return false;
            }
    }
    
    public Boolean UpdateLunchBreakDepartureTime(){
            String sql = "UPDATE tb_point SET LunchBreak = ?, LunchBreakManual = true "
                    + ", DepartureTime = ?, DepartureTimeManual = true "
                    + "WHERE fk_employee = (SELECT idtb_user from tb_user "
                    + "WHERE UserName like? AND UserLastName like ?) AND Date = ?";
            PreparedStatement stm;
            try {
                stm = con.getConexao().prepareStatement(sql);
                stm.setString(1, this.LunchBreak);
                stm.setString(2, this.DepartureTime);
                stm.setString(3, "%" + this.EmployeerName + "%");
                stm.setString(4, "%" + this.EmployeerLastname + "%");
                stm.setString(5, this.Date);
                stm.execute();
                return true;
            }catch (SQLException ex){
                return false;
            }
    }
    
    public Boolean UpdateLunchReturnDepartureTime(){
            String sql = "UPDATE tb_point SET LunchReturn = ?, LunchReturnManual = true "
                    + ", DepartureTime = ?, DepartureTimeManual = true "
                    + "WHERE fk_employee = (SELECT idtb_user from tb_user "
                    + "WHERE UserName like? AND UserLastName like ?) AND Date = ?";
            PreparedStatement stm;
            try {
                stm = con.getConexao().prepareStatement(sql);
                stm.setString(1, this.LunchReturn);
                stm.setString(2, this.DepartureTime);
                stm.setString(3, "%" + this.EmployeerName + "%");
                stm.setString(4, "%" + this.EmployeerLastname + "%");
                stm.setString(5, this.Date);
                stm.execute();
                return true;
            }catch (SQLException ex){
                return false;
            }
    }
}
