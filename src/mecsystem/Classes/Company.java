package mecsystem.Classes;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.InputMismatchException;

public class Company {
    private int idCompany;
    private String CPNJ;
    private String StreetName;
    private String NumberCompany;
    private String NameCompany;
    private String EmailCompany;
    private String EmailPasswordCompany;

    public String getEmailCompany() {
        return EmailCompany;
    }

    public void setEmailCompany(String EmailCompany) {
        this.EmailCompany = EmailCompany;
    }
    Connection con = new Connection();

    public int getIdCompany() {
        return idCompany;
    }

    public void setIdCompany(int idCompany) {
        this.idCompany = idCompany;
    }

    public String getCPNJ() {
        return CPNJ;
    }

    public void setCPNJ(String CPNJ) {
        this.CPNJ = CPNJ;
    }

    public String getStreetName() {
        return StreetName;
    }

    public void setStreetName(String StreetName) {
        this.StreetName = StreetName;
    }

    public String getNumberCompany() {
        return NumberCompany;
    }

    public void setNumberCompany(String NumberCompany) {
        this.NumberCompany = NumberCompany;
    }

    public String getNameCompany() {
        return NameCompany;
    }

    public void setNameCompany(String NameCompany) {
        this.NameCompany = NameCompany;
    }
    
    public void ReadById(){
        ResultSet rs = null;
        java.sql.Connection conect = null;
        conect = con.getConexao();
        String sql = "SELECT * FROM tb_company WHERE idtb_company = ?";
        PreparedStatement stm;
        try {
            stm = conect.prepareStatement(sql);
            stm.setInt(1, this.idCompany);
            rs = stm.executeQuery();
            while(rs.next()){
                this.idCompany = rs.getInt("idtb_company");
                this.CPNJ = rs.getString("CNPJ");
                this.NameCompany = rs.getString("CompanyName");
                String[] Address = rs.getString("Address").split(", ");
                this.NumberCompany = Address[0];
                this.StreetName = Address[1];
            }
        } catch (SQLException ex) {
            System.out.println("Não Consultou"
                    + ex.getMessage());
        }
        
        
        
    } 
    
    public Boolean Create(){
            String sql = "INSERT INTO tb_company ( `idtb_company`, `CNPJ`, `Address`, `CompanyName`, `CompanyEmail`, `CompanyPassword`) VALUES (1, ?, ?, ?, ?, ?)";
            PreparedStatement stm;
            try {
                stm = con.getConexao().prepareStatement(sql);
                stm.setString(1, this.CPNJ);
                stm.setString(2, this.NumberCompany + ", "+ this.StreetName);
                stm.setString(3, this.NameCompany);
                stm.setString(4, this.EmailCompany);
                stm.setString(5, this.EmailPasswordCompany);
                stm.execute();
                return true;
            }catch (SQLException ex){
                return false;
            }
    }

    public String getEmailPasswordCompany() {
        return EmailPasswordCompany;
    }

    public void setEmailPasswordCompany(String EmailPasswordCompany) {
        this.EmailPasswordCompany = EmailPasswordCompany;
    }
    
    public boolean isCNPJ(String CNPJ) {
// considera-se erro CNPJ's formados por uma sequencia de numeros iguais
    if (CNPJ.equals("00000000000000") || CNPJ.equals("11111111111111") ||
        CNPJ.equals("22222222222222") || CNPJ.equals("33333333333333") ||
        CNPJ.equals("44444444444444") || CNPJ.equals("55555555555555") ||
        CNPJ.equals("66666666666666") || CNPJ.equals("77777777777777") ||
        CNPJ.equals("88888888888888") || CNPJ.equals("99999999999999") ||
       (CNPJ.length() != 14))
       return(false);
 
    
    
    
    char dig13, dig14;
    int sm, i, r, num, peso;
 
// "try" - protege o código para eventuais erros de conversao de tipo (int)
    try {
// Calculo do 1o. Digito Verificador
      sm = 0;
      peso = 2;
      for (i=11; i>=0; i--) {
// converte o i-ésimo caractere do CNPJ em um número:
// por exemplo, transforma o caractere '0' no inteiro 0
// (48 eh a posição de '0' na tabela ASCII)
        num = (int)(CNPJ.charAt(i) - 48);
        sm = sm + (num * peso);
        peso = peso + 1;
        if (peso == 10)
           peso = 2;
      }
 
      r = sm % 11;
      if ((r == 0) || (r == 1))
         dig13 = '0';
      else dig13 = (char)((11-r) + 48);
 
// Calculo do 2o. Digito Verificador
      sm = 0;
      peso = 2;
      for (i=12; i>=0; i--) {
        num = (int)(CNPJ.charAt(i)- 48);
        sm = sm + (num * peso);
        peso = peso + 1;
        if (peso == 10)
           peso = 2;
      }
 
      r = sm % 11;
      if ((r == 0) || (r == 1))
         dig14 = '0';
      else dig14 = (char)((11-r) + 48);
 
// Verifica se os dígitos calculados conferem com os dígitos informados.
      if ((dig13 == CNPJ.charAt(12)) && (dig14 == CNPJ.charAt(13)))
         return(true);
      else return(false);
    } catch (InputMismatchException erro) {
        return(false);
    }
  }
    
    
    public String StringtoBraile(String parChar){
        String[] Braile = {"  0 0 0 0 0 0", "a 1 0 0 0 0 0", "b 1 0 1 0 0 0", "c 1 1 0 0 0 0", "d 1 1 0 1 0 0", "e 1 0 0 1 0 0", 
        "f 1 1 1 0 0 0", "g 1 1 1 1 0 0", "h 1 0 1 1 0 0", "i 0 1 1 0 0 0", "j 0 1 1 1 0 0", "k 1 0 0 0 1 0", "l 1 0 1 0 1 0",
        "m 1 1 0 0 1 0", "n 1 1 0 1 1 0", "o 1 0 0 1 1 0", "p 1 1 1 0 1 0", "q 1 1 1 1 1 0", "r 1 0 1 1 1 0", "s 0 1 1 0 1 0", 
        "t 0 1 1 1 1 0", "u 1 0 0 0 1 1", "v 1 0 1 0 1 1", "w 0 1 1 1 0 1", "x 1 1 0 0 1 1", "y 1 1 0 1 1 1", "z 1 0 0 1 1 1",
        "ç 1 1 1 0 1 1", "é 1 1 1 1 1 1", "á 1 1 1 1 1 1", "è 0 1 1 0 1 1", "ú 0 1 1 1 1 1", "â 1 0 0 0 0 1", "ê 1 0 1 0 0 1",
        "í 1 1 0 0 0 1", "ô 1 1 0 1 0 1", "à 1 1 1 0 0 1", ", 0 0 1 0 0 0", ": 0 0 1 1 0 0", "/ 0 0 1 1 0 1", "ó 0 1 0 0 1 1",
        "- 0 0 0 0 1 1", ". 0 0 0 0 1 0", "1 1 0 0 0 0 0", "2 1 0 1 0 0 0", "3 1 1 0 0 0 0", "4 1 1 0 1 0 0", "5 1 0 0 1 0 0", 
        "6 1 1 1 0 0 0", "7 1 1 1 1 0 0", "8 1 0 1 1 0 0", "9 0 1 1 0 0 0", "0 0 1 1 1 0 0"};
        String result = "";
        char[] Word = parChar.toCharArray();
        for(int y = 0; y < Word.length; y++){
            for(int x = 0; x < Braile.length; x++){
                if(Braile[x].startsWith(Character.toString(Word[y]).toLowerCase())){
                    result = result + (Braile[x].substring(1, Braile[x].length()));
                }    

            }
        }
        String[] code = result.split(" ");
        int numColumns = (code.length - 1) / 3;
        String matriz[][] = new String[3][numColumns];
        int parRow = 1;
        int parColumn = 0;
        for (int x = 1; x < code.length; x++) {
            switch (parRow) {
                case 1:
                    matriz[0][0+2*parColumn] = code[x];
                    break;
                case 2:
                    matriz[0][1+2*parColumn] = code[x];
                    break;
                case 3:
                    matriz[1][0+2*parColumn] = code[x];
                    break;
                case 4:
                    matriz[1][1+2*parColumn] = code[x];
                    break;
                case 5:
                    matriz[2][0+2*parColumn] = code[x];
                    break;
                case 6:
                    matriz[2][1+2*parColumn] = code[x];
                    parRow = 0;
                    parColumn++;
                    break;
                default:
                    break;
            }
            parRow++;
        }
        String FinalBraile = "";
        for(int x = 0; x < matriz.length; x++){
            for(int i = 0; i < matriz[x].length; i++){
                FinalBraile = FinalBraile + matriz[x][i] +" ";
            }
            FinalBraile = FinalBraile + "\n";
        }
        FinalBraile = FinalBraile.replace("1", ".");
        FinalBraile = FinalBraile.replace("0", " ");
        return FinalBraile;
    }
}
